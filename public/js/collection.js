var $collectionHolder;
var $collectionCommander;
var $collectionVente;

var $addProduitButton = $('<div class="text-right"><button type="button" class="add_produit_link btn btn-primary" title="Ajouter un produit" data-toggle="tooltip"><i class="fa fa-plus"></i></button></div>');
var $newLinkLi = $('<li></li>').append($addProduitButton);
var $addCommandeButton = $('<button type="button" class="add_commande_link" >Ajouter des détails à la commande</button>');
var $newCommandeLinkLi = $(' <div class="col-md-12"></div>').append($addCommandeButton);
var $addVenteButton = $('<button type="button" class="add_Vente_link" >Ajouter des détails à la vente</button>');
var $newVenteLinkLi = $('<li></li>').append($addVenteButton);

$(document).ready(function () {
    $('#produitCollection').hide();
    var confirme;
    $("input[name='confirmation']").click(function () {
        confirme =  $("input[name='confirmation']:checked").val();

        if (confirme === 'oui')
        {
            $('#produitCollection').show();
        } else
        {
            $('#produitCollection').hide();
        }
    });

    $collectionHolder = $("ul.produits");

    $collectionHolder.append($newLinkLi);

    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addProduitButton.on('click', function (e) {
        addProduitForm($collectionHolder, $newLinkLi);
    });


    // $collectionHolder.find('li').each(function () {
    //     addProduitFormDeleteLink($(this));
    // })


    function addProduitForm($collectionHolder, $newLinkLi) {
        var prototype = $collectionHolder.data('prototype');

        var index = $collectionHolder.data('index');

        var newForm = prototype;

        newForm = newForm.replace(/_name_/g, index);

        $collectionHolder.data('index', index+1);

        var $newFormLi = $('<li></li>').append(newForm);
        $newLinkLi.before($newFormLi)
        addFormDeleteLink($newFormLi);
    }


    function addFormDeleteLink($formLi) {
        var $removeFormButton = $('<button type="button" class="btn btn-danger">Supprmer</button>');
        $formLi.append($removeFormButton);

        $removeFormButton.on('click', function (e) {
            $formLi.remove();
        })
    }


/***********************Commandes*****************************/

    $collectionCommander = $("div.commander");

    $collectionCommander.append($newCommandeLinkLi);

    $collectionCommander.data('index', $collectionCommander.find(':input').length);

    $addCommandeButton.on('click', function (e) {
        addCommanderForm($collectionCommander, $newCommandeLinkLi);
    });


    function addCommanderForm($collectionCommander, $newCommandeLinkLi) {
        var prototype = $collectionCommander.data('prototype');

        var index = $collectionCommander.data('index');

        var newCommandeForm = prototype;

        newCommandeForm = newCommandeForm.replace(/_name_/g, index);

        $collectionCommander.data('index', index+1);

        var $newCommandeFormLi = $('<div class="row"><div class="col-md-12"></div></div>').append(newCommandeForm);
        $newCommandeLinkLi.before($newCommandeFormLi);
        addFormDeleteLink($newCommandeFormLi);
    }


    /**************************Vente**************************************/

    $collectionVente = $("ul.vente");

    $collectionVente.append($newVenteLinkLi);

    $collectionVente.data('index', $collectionVente.find(':input').length);

    $addVenteButton.on('click', function (e) {
        addCommanderForm($collectionVente, $newVenteLinkLi);
    });


    function addVenteForm($collectionVente, $newVenteLinkLi) {
        var prototype = $collectionVente.data('prototype');

        var index = $collectionVente.data('index');

        var newVenteForm = prototype;

        newVenteForm = newVenteForm.replace(/_name_/g, index);

        $collectionVente.data('index', index+1);

        var $newVenteFormLi = $('<li></li>').append(newVenteForm);
        $newVenteLinkLi.before($newVenteLinkLi);
        addFormDeleteLink($newVenteLinkLi);
    }
});