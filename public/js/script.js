
$(document).ready(function () {
    
    $(function () {
        $('#clientTable').DataTable();
        $('#categorieTable').DataTable();
        $('#fournisseurTable').DataTable();
        $('#produitTable').DataTable();
        $('#entreeTable').DataTable();
        $('#sortieTable').DataTable();
        $('#detail').DataTable();
    });

    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });

});