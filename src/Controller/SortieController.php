<?php

namespace App\Controller;

use App\Entity\Sortie;
use App\Form\SortieType;
use App\Repository\LivrerRepository;
use App\Repository\ProduitRepository;
use App\Repository\SortieRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SortieController
 * @package App\Controller
 * @Route("/sortie")
 */
class SortieController extends AbstractController
{

    /**
     * @var ProduitRepository
     */
    private $produitRepository;

    /**
     * @var SortieRepository
     */
    private $sortieRepository;

    /**
     * ProduitController constructor.
     * @param ProduitRepository $produitRepository
     */
    public function __construct(ProduitRepository $produitRepository, SortieRepository $sortieRepository)
    {
        $this->produitRepository = $produitRepository;
        $this->sortieRepository = $sortieRepository;
    }

    /**
     * @Route("/", name="sortie_index")
     */
    public function index()
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();
        $sorties = $this->sortieRepository->findAll();



        return $this->render('sortie/index.html.twig', [
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
            'sorties'=>$sorties,
        ]);
    }

    /**
     * @Route("/new", name="sortie_new")
     */
    public function new(Request $request, ObjectManager $manager)
    {
        $sortie = new Sortie();
        $form = $this->createForm(SortieType::class, $sortie);
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $livrers = $sortie->getLivrers();
            ;
            foreach ($livrers as $livrer)
            {
                $quantiteLivrer =  $livrer->getQuantiteLivrer();
                $quantiteDisponible =  $livrer->getProduit()->getQuantiteDisponible();
                if ($quantiteLivrer > $quantiteDisponible)
                {
                    echo 'Vous n\'avez pas assez de produit en stock'.$quantiteDisponible;
                    return $this->redirectToRoute("sortie_new");
                }
                $prixVente =  $livrer->getProduit()->getPrixDeVente();
                $montant = $prixVente * $quantiteLivrer;
                $livrer->setMontant($montant);
                $quantiteRestante = $quantiteDisponible - $quantiteLivrer;
                $livrer->getProduit()->setQuantiteDisponible($quantiteRestante);

            }

            $manager->persist($sortie);
            $manager->flush();
            $this->addFlash('success', 'Vente effectuée avec succès');
            return $this->redirectToRoute('sortie_index');

        }

        return $this->render('sortie/new.html.twig',[
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
            'form'=>$form->createView(),
        ]);
    }

    /**
     * @param Sortie $sortie
     * @param SortieRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{id}/show", name="sorie_show")
     */
    public function show(Sortie $sortie, LivrerRepository $livrerRepository)
    {

        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();
        $detailVente = $livrerRepository->findDetailVente($sortie);

        return $this->render('sortie/show.html.twig',[
            'detailVente'=>$detailVente,
            'stockMinimal'=>$stockMinimal,
            'seuilAlert'=>$seuilAlert,
            'sortie' => $sortie,
        ]);
    }

    /**
     * @param Request $request
     * @param Sortie $sortie
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{id}/delete", name="sortie_delete")
     */
    public function delete(Request $request, Sortie $sortie, ObjectManager $manager)
    {
        if ($this->isCsrfTokenValid('delete'.$sortie->getId(), $request->request->get('_token'))) {
            $manager->remove($sortie);
            $manager->flush();
            $this->addFlash('success', 'Vente supprimée avec succès');
        }

        return $this->redirectToRoute('sortie_index');
    }
}
