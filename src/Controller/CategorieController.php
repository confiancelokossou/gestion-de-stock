<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Form\Categorie1Type;
use App\Form\CategorieType;
use App\Repository\CategorieRepository;
use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategorieController
 * @package App\Controller
 * @Route("categorie")

 */
class CategorieController extends AbstractController
{
    /**
     * @var ProduitRepository
     */
    private $produitRepository;

    /**
     * CategorieController constructor.
     * @param ProduitRepository $produitRepository
     */
    public function __construct(ProduitRepository $produitRepository)
    {
        $this->produitRepository = $produitRepository;
    }

    /**
     * @Route("/", name="categorie_index")
     */
    public function index(CategorieRepository $categorieRepository)
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();
        $categories = $categorieRepository->findAll();
        return $this->render('categorie/index.html.twig', [
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
            'categories'=>$categories,
        ]);
    }

    /**
     * @Route("/new", name="categorie_new")
     */
    public  function new(Request $request, ObjectManager $manager)
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();
        $categorie = new Categorie();
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $manager->persist($categorie);
            $manager->flush();
            $this->addFlash('success', 'Catégorie ajoutée avec succès');
            return $this->redirectToRoute('categorie_index');
        }

     return $this->render("categorie/new.html.twig", [
         'form'=>$form->createView(),
         'seuilAlert'=>$seuilAlert,
         'stockMinimal'=>$stockMinimal,

     ]);
    }

    /**
     * @param Categorie $categorie
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{id}/edit", name="categorie_edit")
     */
    public function edit(Categorie $categorie, ObjectManager $manager, Request $request)
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();

        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $manager->flush();
            $this->addFlash('success', 'Caégorie modifiée avec succès');
            return $this->redirectToRoute('categorie_index');
        }

        return $this->render('categorie/edit.html.twig',[
            'categorie'=>$categorie,
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
            'form'=>$form->createView(),
            ]);
    }


    /**
     * @param Categorie $categorie
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{id}/delete", name="categorie_delete")
     */
    public function delete(Categorie $categorie, Request $request, ObjectManager $manager)
    {
        if ($this->isCsrfTokenValid('delete'.$categorie->getId(), $request->request->get('_token'))) {
            $manager->remove($categorie);
            $manager->flush();
            $this->addFlash('success', 'Catégorie supprimée avec succès');
        }

        return $this->redirectToRoute('categorie_index');
    }
}
