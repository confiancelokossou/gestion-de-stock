<?php

namespace App\Controller;

use App\Form\EntreeSearchType;
use App\Repository\EntreeRepository;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchCommandeController extends AbstractController
{

    private $produitRepository;

    public function __construct(ProduitRepository $produitRepository)
    {
        $this->produitRepository = $produitRepository;
    }

    /**
     * @Route("/search/commande", name="search_commande")
     */
    public function index(Request $request, EntreeRepository $entree)
    {
        $entrees = [];
        $form = $this->createForm(EntreeSearchType::class);

        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            dump($form->getData());
            $entrees = $entree->FindCommandeByFournisseurAndPeriode($form->getData());
        }

        return $this->render('search_commande/index.html.twig', [
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
            'form'=>$form->createView(),
            'entrees'=>$entrees
        ]);
    }
}
