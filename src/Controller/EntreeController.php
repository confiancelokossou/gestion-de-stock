<?php

namespace App\Controller;

use App\Entity\DetailProduitEntree;
use App\Entity\Entree;
use App\Entity\EntreeSearch;
use App\Entity\Produit;
use App\Form\EntreeSearchType;
use App\Form\EntreeType;
use App\Form\FournisseurType;
use App\Repository\CommanderRepository;
use App\Repository\EntreeRepository;
use App\Repository\ProduitRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EntreeController
 * @package App\Controller
 * @Route("/entree")
 */
class EntreeController extends AbstractController
{
    /**
     * @var ProduitRepository
     */
    private $produitRepository;
    private $manager;
    private $entreeRepository;
    private $commandeRepository;

    /**
     * CategorieController constructor.
     * @param ProduitRepository $produitRepository
     */
    public function __construct(ProduitRepository $produitRepository, ObjectManager $manager, EntreeRepository $entreeRepository, CommanderRepository $commandeRepository)
    {
        $this->produitRepository = $produitRepository;
        $this->manager = $manager;
        $this->entreeRepository = $entreeRepository;
        $this->commandeRepository = $commandeRepository;
    }

    /**
     * @Route("/", name="entree_index")
     *
     */
    public function index(Request $request)
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();
        $entrees = $this->entreeRepository->findAll();


        return $this->render('entree/index.html.twig', [
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
            'entrees'=>$entrees,
        ]);
    }

    /**
     * @Route("/{id}/show", name="entree_detail")
     */
    public function show(Entree $entree)
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();
        $detailCommande = $this->commandeRepository->detailCommande( $entree);
        $entrees = $this->entreeRepository->findAll();

        return $this->render('entree/show.html.twig',[
            'detailCommande'=>$detailCommande,
            'stockMinimal'=>$stockMinimal,
            'seuilAlert'=>$seuilAlert,
            'entree' => $entree,
        ]);
    }

    /**
     * @Route("/new", name="entree_new")
     */
    public function new(Request $request)
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
        $stockMinimal = $this->produitRepository->stockMinimal();
        $entree = new Entree();
        $form = $this->createForm(EntreeType::class, $entree);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
               $commandes = $entree->getCommanders();

               foreach ($commandes as $commande){
                  // dump($commande->getProduit()->getName());
                   $quantiteCommande = $commande->getQuantiteCommander();
                   $quantiteDisponible = $commande->getProduit()->getQuantiteDisponible();
                   $total = $quantiteCommande + $quantiteDisponible;
                   $produit =  $commande->getProduit();
                   $idProduit =  $commande->getProduit()->getId();
                   $produit->setQuantiteDisponible($total);
               }

            //dd($entree);

            //dd($entree->getCommanders()->getValues(['quantiteCommander']));
            $this->manager->persist($entree);
           // $this->manager->persist($dpe);
            $this->manager->flush();
            $this->addFlash('success', 'Commande effectuée avec succès');
            return $this->redirectToRoute('entree_index');
        }

        return $this->render('entree/new.html.twig', [
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
            'form'=>$form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Entree $entree
     * @Route("/{id}/delete", name="entree_delete")
     */
    public function delete(Request $request, Entree $entree, ObjectManager $manager)
    {
        if ($this->isCsrfTokenValid('delete'.$entree->getId(), $request->request->get('_token'))) {
            $manager->remove($entree);
            $manager->flush();
            $this->addFlash('success', 'Commande supprimée avec succès');
        }

        return $this->redirectToRoute('entree_index');
    }
}
