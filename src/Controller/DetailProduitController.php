<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DetailProduitController extends AbstractController
{

    /**
     * @var ProduitRepository
     */
    private $produitRepository;

    /**
     * HomeController constructor.
     * @param ProduitRepository $produitRepository
     */
    public function __construct(ProduitRepository $produitRepository)
    {
        $this->produitRepository = $produitRepository;
    }
    /**
     * @Route("/detail/produit", name="detail_produit")
     */
    public function index()
    {
        $seuilAlert = $this->produitRepository->seuilAlert();
       $stockMinimal = $this->produitRepository->stockMinimal();
        return $this->render('detail_produit/index.html.twig', [
            'seuilAlert'=>$seuilAlert,
            'stockMinimal'=>$stockMinimal,
        ]);
    }

}
