<?php

namespace App\Repository;

use App\Entity\Livrer;
use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Livrer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Livrer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Livrer[]    findAll()
 * @method Livrer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LivrerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Livrer::class);
    }


    public function findDetailVente(Sortie $sortie)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.sortie = :sortie')
            ->setParameter('sortie', $sortie->getId())
            ->getQuery()
            ->getResult()
            ;
    }
    // /**
    //  * @return Livrer[] Returns an array of Livrer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Livrer
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
