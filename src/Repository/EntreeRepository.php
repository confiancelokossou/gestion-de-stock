<?php

namespace App\Repository;

use App\Entity\Entree;
use App\Entity\EntreeSearch;
use App\Form\EntreeSearchType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Entree|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entree|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entree[]    findAll()
 * @method Entree[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntreeRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Entree::class);
    }


    public function FindCommandeByFournisseurAndPeriode(EntreeSearch $entreeSearch)
    {
        return $this->createQueryBuilder('p')
                ->andWhere('p.createdAt BETWEEN :dateDebut AND :dateFin')
                ->setParameters([
                    'dateDebut'=>$entreeSearch->getDateDebut(),
                    'dateFin'=>$entreeSearch->getDateFin(),
                ])
                ->getQuery()
                ->getResult()
            ;

      }


//public function FindCommandeByFournisseurAndPeriode(EntreeSearch $entreeSearch)
//    {
//        return $this->createQueryBuilder('p')
//
//                ->where('p.fournisseur = :fournisseur')
//                ->setParameter('fournisseur', $entreeSearch->getFournisseur())
//                ->andWhere('p.createdAt >= :dateDebut')
//                ->setParameter('dateDebut', $entreeSearch->getDateDebut())
//                ->andWhere('p.createdAt <= :dateFin')
//                ->setParameter('dateFin', $entreeSearch->getDateFin())
//                ->getQuery()
//                ->getResult()
//
//            ;
//
//    }



    // /**
    //  * @return Entree[] Returns an array of Entree objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Entree
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
