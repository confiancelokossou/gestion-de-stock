<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LivrerRepository")
 */
class Livrer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantiteLivrer;

    /**
     * @ORM\Column(type="integer")
     */
    private $montant;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="livrers")
     */
    private $produit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sortie", inversedBy="livrers")
     */
    private $sortie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantiteLivrer(): ?int
    {
        return $this->quantiteLivrer;
    }

    public function setQuantiteLivrer(int $quantiteLivrer): self
    {
        $this->quantiteLivrer = $quantiteLivrer;

        return $this;
    }

    public function getMontant(): ?int
    {
        return $this->montant;
    }

    public function setMontant(int $montant): self
    {
        $this->montant = $montant;

        return $this;
    }


    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getSortie(): ?Sortie
    {
        return $this->sortie;
    }

    public function setSortie(?Sortie $sortie): self
    {
        $this->sortie = $sortie;

        return $this;
    }
}
