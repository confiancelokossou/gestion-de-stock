<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommanderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Commander
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantiteCommander;

    /**
     * @ORM\Column(type="integer")
     */
    private $montant;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixAchatUnitaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="commanders")
     */
    private $produit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entree", inversedBy="commanders")
     */
    private $entree;



    public function __construct()
    {
        $this->produits = new ArrayCollection();
        $this->entree = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantiteCommander(): ?int
    {
        return $this->quantiteCommander;
    }

    public function setQuantiteCommander(int $quantiteCommander): self
    {
        $this->quantiteCommander = $quantiteCommander;

        return $this;
    }

    public function getMontant(): ?int
    {
        return $this->montant;
    }

    public function setMontant(int $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setMontantAchat()
    {
        $this->montant = $this->prixAchatUnitaire * $this->quantiteCommander;
    }

    public function getPrixAchatUnitaire(): ?int
    {
        return $this->prixAchatUnitaire;
    }

    public function setPrixAchatUnitaire(int $prixAchatUnitaire): self
    {
        $this->prixAchatUnitaire = $prixAchatUnitaire;

        return $this;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getEntree(): ?Entree
    {
        return $this->entree;
    }

    public function setEntree(?Entree $entree): self
    {
        $this->entree = $entree;

        return $this;
    }

}
