<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Produit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantiteDisponible;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixDeVente;

    /**
     * @ORM\Column(type="integer")
     */
    private $valeurDuStock;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stockMinimal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $seuilAlert;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="produits")
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commander", mappedBy="produit")
     */
    private $commanders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Livrer", mappedBy="produit")
     */
    private $livrers;


    public function __construct()
    {
        $this->sorties = new ArrayCollection();
        $this->commanders = new ArrayCollection();
        $this->livrers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getQuantiteDisponible(): ?int
    {
        return $this->quantiteDisponible;
    }

    public function setQuantiteDisponible(int $quantiteDisponible): self
    {
        $this->quantiteDisponible = $quantiteDisponible;

        return $this;
    }

    public function getPrixDeVente(): ?int
    {
        return $this->prixDeVente;
    }

    public function setPrixDeVente(int $prixDeVente): self
    {
        $this->prixDeVente = $prixDeVente;

        return $this;
    }

    public function getValeurDuStock(): ?int
    {
        return $this->valeurDuStock;
    }


    public function setValeurDuStock(int $valeurDuStock): self
    {
        $this->valeurDuStock = $valeurDuStock;

        return $this;
    }

    /**
     * methode appellée chaque fois qu'il y'aura un persist sur cette methode
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setValeurStock()
    {
        $this->valeurDuStock = $this->prixDeVente * $this->quantiteDisponible;
    }


    public function getStockMinimal(): ?int
    {
        return $this->stockMinimal;
    }

    public function setStockMinimal(?int $stockMinimal): self
    {
        $this->stockMinimal = $stockMinimal;

        return $this;
    }

    public function getSeuilAlert(): ?int
    {
        return $this->seuilAlert;
    }

    public function setSeuilAlert(?int $seuilAlert): self
    {
        $this->seuilAlert = $seuilAlert;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function addCategorie(Categorie $categorie)
    {
        if (!$this->categorie->contains($categorie)){
            $this->categorie->add($categorie);
        }
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }

    /**
     * @return Collection|Commander[]
     */
    public function getCommanders(): Collection
    {
        return $this->commanders;
    }

    public function addCommander(Commander $commander): self
    {
        if (!$this->commanders->contains($commander)) {
            $this->commanders[] = $commander;
            $commander->setProduit($this);
        }

        return $this;
    }

    public function removeCommander(Commander $commander): self
    {
        if ($this->commanders->contains($commander)) {
            $this->commanders->removeElement($commander);
            // set the owning side to null (unless already changed)
            if ($commander->getProduit() === $this) {
                $commander->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Livrer[]
     */
    public function getLivrers(): Collection
    {
        return $this->livrers;
    }

    public function addLivrer(Livrer $livrer): self
    {
        if (!$this->livrers->contains($livrer)) {
            $this->livrers[] = $livrer;
            $livrer->setProduit($this);
        }

        return $this;
    }

    public function removeLivrer(Livrer $livrer): self
    {
        if ($this->livrers->contains($livrer)) {
            $this->livrers->removeElement($livrer);
            // set the owning side to null (unless already changed)
            if ($livrer->getProduit() === $this) {
                $livrer->setProduit(null);
            }
        }

        return $this;
    }

}
