<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @UniqueEntity(
 *     fields={"name", "telephone", "mail"},
 *     message="Ce client existe déjà"
 * )
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique= true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer", unique= true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=255, unique= true)
     */
    private $mail;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sortie", mappedBy="client")
     */
    private $sorties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Livrer", mappedBy="client")
     */
    private $livrers;


    public function __construct()
    {
        $this->sorties = new ArrayCollection();
        $this->livrers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTelephone(): ?int
    {
        return $this->telephone;
    }

    public function setTelephone(int $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * @return Collection|Sortie[]
     */
    public function getSorties(): Collection
    {
        return $this->sorties;
    }

    public function addSorty(Sortie $sorty): self
    {
        if (!$this->sorties->contains($sorty)) {
            $this->sorties[] = $sorty;
            $sorty->setClient($this);
        }

        return $this;
    }

    public function removeSorty(Sortie $sorty): self
    {
        if ($this->sorties->contains($sorty)) {
            $this->sorties->removeElement($sorty);
            // set the owning side to null (unless already changed)
            if ($sorty->getClient() === $this) {
                $sorty->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Livrer[]
     */
    public function getLivrers(): Collection
    {
        return $this->livrers;
    }

    public function addLivrer(Livrer $livrer): self
    {
        if (!$this->livrers->contains($livrer)) {
            $this->livrers[] = $livrer;
            $livrer->setClient($this);
        }

        return $this;
    }

    public function removeLivrer(Livrer $livrer): self
    {
        if ($this->livrers->contains($livrer)) {
            $this->livrers->removeElement($livrer);
            // set the owning side to null (unless already changed)
            if ($livrer->getClient() === $this) {
                $livrer->setClient(null);
            }
        }

        return $this;
    }


    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }
}
