<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SortieRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Sortie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="sorties")
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Livrer", mappedBy="sortie", cascade={"persist", "remove"})
     */
    private $livrers;


    public function __construct()
    {
        $this->produits = new ArrayCollection();
        $this->livrers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     * @throws \Exception
     * @ORM\PrePersist()
     */
    public function dateSortie()
    {
       return $this->createdAt = new \DateTime("now");
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|Livrer[]
     */
    public function getLivrers(): Collection
    {
        return $this->livrers;
    }

    public function addLivrer(Livrer $livrer): self
    {
        if (!$this->livrers->contains($livrer)) {
            $this->livrers[] = $livrer;
            $livrer->setSortie($this);
        }

        return $this;
    }

    public function removeLivrer(Livrer $livrer): self
    {
        if ($this->livrers->contains($livrer)) {
            $this->livrers->removeElement($livrer);
            // set the owning side to null (unless already changed)
            if ($livrer->getSortie() === $this) {
                $livrer->setSortie(null);
            }
        }

        return $this;
    }

}
