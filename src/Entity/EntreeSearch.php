<?php

namespace App\Entity;

class EntreeSearch
{

    /**
     * @var string|null
     */
    private $fournisseur;

    /**
     * @var \DateTime|null
     */
    private $dateDebut;

    /**
     * @var \DateTime|null
     */
    private $dateFin;


    /**
     * @return string|null
     */
    public function getFournisseur(): ?string
    {
        return $this->fournisseur;
    }

    /**
     * @param string|null $fournisseur
     * @return EntreeSearch
     */
    public function setFournisseur(?string $fournisseur): EntreeSearch
    {
        $this->fournisseur = $fournisseur;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateDebut(): ?\DateTime
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime|null $dateDebut
     * @return EntreeSearch
     */
    public function setDateDebut(?\DateTime $dateDebut): EntreeSearch
    {
        $this->dateDebut = $dateDebut;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateFin(): ?\DateTime
    {
        return $this->dateFin;
    }

    /**
     * @param \DateTime|null $dareFin
     * @return EntreeSearch
     */
    public function setDateFin(?\DateTime $dateFin): EntreeSearch
    {
        $this->dateFin = $dateFin;
        return $this;
    }

}