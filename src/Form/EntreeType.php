<?php

namespace App\Form;

use App\Entity\Entree;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label'=>'Description de la commande:'))
            ->add('createdAt', DateType::class, [
                'label'=>'Date de la commande',
                'format'=> 'dd-MMMM-yyyy'
            ])
            ->add('fournisseur')
        ;

        $builder
            ->add('commanders', CollectionType::class,[
            'entry_type' => CommanderType::class,
                'entry_options' => ['label' => false],
                'allow_add' =>true,
                'by_reference' => false,
                'allow_delete' =>true,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entree::class,
        ]);
    }
}
