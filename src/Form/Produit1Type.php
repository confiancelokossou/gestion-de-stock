<?php

namespace App\Form;

use App\Entity\Produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Produit1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label'=>'Nom du prduit:'])
            ->add('quantiteDisponible', IntegerType::class, ['label'=>'Quantité disponible en stock:'])
            ->add('prixDeVente', IntegerType::class, ['label'=>'Prix de vente du produit:'])
            ->add('stockMinimal', IntegerType::class, ['label'=>'Stock minimal du produit:'])
            ->add('seuilAlert', IntegerType::class, ['label'=>'Seuil alerte du produit:'])
            ->add('categorie')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
