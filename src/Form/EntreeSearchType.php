<?php

namespace App\Form;

use App\Entity\EntreeSearch;
use App\Entity\Fournisseur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;

class EntreeSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fournisseur', EntityType::class, [
                "class"=>Fournisseur::class,
                "label"=>"fournisseur",
                "required"=>false,
            ])
            ->add('dateDebut', DateType::class, [
                'widget'=>'single_text',
            ])
            ->add('dateFin', DateType::class,
                [
                    'widget'=>'single_text',
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EntreeSearch::class,
        ]);
    }

}

